package com.si.anpr;

import com.sun.jna.Structure;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;

import java.util.ArrayList;
import java.util.List;

//import org.opencv.core.Core;
//import org.opencv.core.CvType;
//import org.opencv.core.Mat;
//import org.opencv.core.MatOfPoint;
//import org.opencv.core.Point;
//import org.opencv.core.Scalar;
//import org.opencv.imgproc.Imgproc;

/**
 * Created by martoniczd on 2016. 12. 17..
 */
public class Cardet {


    static {
        try {
            NativeUtils.loadLibraryFromJar("/libanpr.so");
            NativeUtils.loadLibraryFromJar("/libcardetjni.so");
            //System.loadLibrary(op);
            //NativeUtils.loadLibraryFromJar("/libopencv_core.so");

        } catch (Exception e) {
            System.out.println("Exception in Cardet loading!");
        }
    }

    public native int Init(int __InterfaceVersion);

    public native String ProcessImage(byte[] image, int width, int height);

    public native int Close();

//    private static Logger logger = LoggerFactory.getLogger(Cardet.class);
//    public static final int DETECT_WHITE_ON_BLACK = 1;
//    public static final int USE_LIGHT_CORRECTION = 1;
//    public static final int MULTIPLE_RECOGNITION_LIMIT = 3;
//    public static final int DETECT_SQUARE_PLACE = 1;
//
//    public void Init(){
//        this.CardetInit(2);
//    }
//
//    static {
//        //System.loadLibrary("anpr");
//        try {
//            NativeUtils.loadLibraryFromJar("/anpr.so");
//            //NativeUtils.loadLibraryFromJar("/opencv_java2413.dll");
//            //return (anprHU)Native.loadLibrary(anprHU.class);
//        }
//        catch (Throwable throwable) {
//            //logger.error("Error during loading ANPR " + throwable);
//
//        }
//    }
//
//    public native int CardetInit(int var1);
//
//    public native int CardetProcessImage(ENGINE_INDATA var1, int var2, ENGINE_OUTDATA var3);
//
//    public native int CardetClose();

    public static enum tCardetReturn {
        ENGINE_ERR_BADINDEX(14),
        ENGINE_ERR_BADPARAM(13),
        ENGINE_ERR_EXCEPTION(12),
        ENGINE_ERR_NOINIT(11),
        ENGINE_ERR_NOBUFFER(10),
        ENGINE_PLATE_FOUND(0),
        ENGINE_PLATE_NOTFOUND(1);

        private final int value;

        private tCardetReturn(int newValue) {
            this.value = newValue;
        }

        public int getValue() {
            return this.value;
        }
    }

    public static class ENGINE_OUTDATA
            extends Structure {
        public int StructSize;
        public int Version;
        public byte[] CharBuffer = new byte[12];
        public int NumberOfChars;
        public IntByReference CharPositions = new IntByReference();
        public int Confidence;
        public int DetectionMethod;
        public int bWhiteOnBlack;
        public int[] CharConfidences = new int[10];
        public int x0;
        public int y0;
        public int x1;
        public int y1;
        public int x2;
        public int y2;
        public int x3;
        public int y3;
        public int PlateWidth;
        public int PlateHeight;
        public int reserved1;
        public int reserved2;
        public int reserved3;
        public int reserved4;
        public int reserved5;
        public IntByReference pPatchBuffer;
        public int PatchBufferSpan;
        public int PatchHeight;
        public int PatchWidth;
        public int Country;
        public int reserved6;
        public int reserved7;
        public int AvgCharHeight;
        public int SyntaxWeight;
        public int SyntaxCode;
        public int Contrast;
        public int reserved8;
        public int reserved9;
        public short BlackLevel;
        public short WhiteLevel;
        public int SyntaxSubCode;
        public int ConfidenceCategory;
        public int SyntaxConfidenceCategory;
        public byte[] SyntaxName = new byte[8];
        public int reserved10;
        public int reserved11;
        public int reserved12;
        public int reserved13;

        protected List getFieldOrder() {
            ArrayList<String> list = new ArrayList<String>();
            list.add("StructSize");
            list.add("Version");
            list.add("CharBuffer");
            list.add("NumberOfChars");
            list.add("CharPositions");
            list.add("Confidence");
            list.add("DetectionMethod");
            list.add("bWhiteOnBlack");
            list.add("CharConfidences");
            list.add("x0");
            list.add("y0");
            list.add("x1");
            list.add("y1");
            list.add("x2");
            list.add("y2");
            list.add("x3");
            list.add("y3");
            list.add("PlateWidth");
            list.add("PlateHeight");
            list.add("reserved1");
            list.add("reserved2");
            list.add("reserved3");
            list.add("reserved4");
            list.add("reserved5");
            list.add("pPatchBuffer");
            list.add("PatchBufferSpan");
            list.add("PatchHeight");
            list.add("PatchWidth");
            list.add("Country");
            list.add("reserved6");
            list.add("reserved7");
            list.add("AvgCharHeight");
            list.add("SyntaxWeight");
            list.add("SyntaxCode");
            list.add("Contrast");
            list.add("reserved8");
            list.add("reserved9");
            list.add("BlackLevel");
            list.add("WhiteLevel");
            list.add("SyntaxSubCode");
            list.add("ConfidenceCategory");
            list.add("SyntaxConfidenceCategory");
            list.add("SyntaxName");
            list.add("reserved10");
            list.add("reserved11");
            list.add("reserved12");
            list.add("reserved13");
            return list;
        }
    }

    public static class ENGINE_INDATA
            extends Structure {
        public ByteByReference pImageBuffer;
        public int ImageWidth;
        public int ImageHeight;
        public int ImageBufferSpan;
        public int AspectRatioX;
        public int AspectRatioY;
        public int bUseLightCorrection;
        public int bDetectSquarePlates;
        public int bDetectWhiteOnBlack;
        public int AdvancedOptions;
        public int MultipleRecognLimit;

        protected List getFieldOrder() {
            ArrayList<String> list = new ArrayList<String>();
            list.add("pImageBuffer");
            list.add("ImageWidth");
            list.add("ImageHeight");
            list.add("ImageBufferSpan");
            list.add("AspectRatioX");
            list.add("AspectRatioY");
            list.add("bUseLightCorrection");
            list.add("bDetectSquarePlates");
            list.add("bDetectWhiteOnBlack");
            list.add("AdvancedOptions");
            list.add("MultipleRecognLimit");
            return list;
        }
    }
}
/*
    public static class ANPRLoader {
        public static anprHU load() {
            try {
                NativeUtils.loadLibraryFromJar("/anpr.so");
                //NativeUtils.loadLibraryFromJar("/opencv_java2413.dll");
                return (anprHU)Native.loadLibrary(anprHU.class);
            }
            catch (Throwable throwable) {
                //logger.error("Error during loading ANPR " + throwable);
                return null;
            }
        }

    }
*/
/*
    public List<String> recognize(byte[] img) {
        ArrayList plateNumbers = new ArrayList<String>();
        ByteArrayInputStream in = new ByteArrayInputStream(img);
        try {
            BufferedImage image = ImageIO.read(in);
            anprHU.ENGINE_INDATA indata = new anprHU.ENGINE_INDATA();
            anprHU.ENGINE_OUTDATA out = new anprHU.ENGINE_OUTDATA();
            int result = 0;
            byte[] data = ((DataBufferByte)image.getRaster().getDataBuffer()).getData();
            Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
            mat.put(0, 0, data);
            Mat mat1 = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC1);
            Imgproc.cvtColor((Mat)mat, (Mat)mat1, (int)7);
            mat.release();
            byte[] grayScaledImage = new byte[mat1.rows() * mat1.cols() * (int)mat1.elemSize()];
            mat1.get(0, 0, grayScaledImage);
            BufferedImage image1 = new BufferedImage(mat1.cols(), mat1.rows(), 10);
            image1.getRaster().setDataElements(0, 0, mat1.cols(), mat1.rows(), grayScaledImage);
            indata.AdvancedOptions = 0;
            indata.AspectRatioX = 1;
            indata.AspectRatioY = 1;
            ByteByReference intByReference = new ByteByReference();
            Memory pointer = new Memory((long)grayScaledImage.length);
            pointer.write(0, grayScaledImage, 0, grayScaledImage.length);
            intByReference.setPointer((Pointer)pointer);
            indata.bDetectSquarePlates = 1;
            indata.pImageBuffer = intByReference;
            indata.bDetectWhiteOnBlack = 1;
            indata.bUseLightCorrection = 1;
            indata.ImageBufferSpan = image.getWidth();
            indata.MultipleRecognLimit = 3;
            indata.ImageHeight = image.getHeight();
            indata.ImageWidth = image.getWidth();
            boolean loopAgain = true;
            for (int detectionAttempts = 0; detectionAttempts < 3 && loopAgain; ++detectionAttempts) {
                //CardetService cardetService = this;
                //synchronized (cardetService) {
                //    result = this.sdll.CardetProcessImage(indata, 0, out);
                //}
                if (result == anprHU.tCardetReturn.ENGINE_PLATE_FOUND.getValue()) {
                    //plateNumbers.add(this.getPlateNumber(out.CharBuffer));
                    //ArrayList points = Lists.newArrayList();
//                    //MatOfPoint matOfPoint = new MatOfPoint();
//                    points.add(new Point((double)out.x0, (double)out.y0));
//                    points.add(new Point((double)out.x1, (double)out.y1));
//                    points.add(new Point((double)out.x2, (double)out.y2));
//                    points.add(new Point((double)out.x3, (double)out.y3));
//                    matOfPoint.fromList((List)points);
//                    Core.fillPoly((Mat)mat1, (List)Lists.newArrayList((Object[])new MatOfPoint[]{matOfPoint}), (Scalar)new Scalar(0.0, 0.0, 0.0));
//                    grayScaledImage = new byte[mat1.rows() * mat1.cols() * (int)mat1.elemSize()];
//                    mat1.get(0, 0, grayScaledImage);
//                    image1 = new BufferedImage(mat1.cols(), mat1.rows(), 10);
//                    image1.getRaster().setDataElements(0, 0, mat1.cols(), mat1.rows(), grayScaledImage);
//                    intByReference = new ByteByReference();
//                    pointer = new Memory((long)grayScaledImage.length);
//                    pointer.write(0, grayScaledImage, 0, grayScaledImage.length);
//                    intByReference.setPointer((Pointer)pointer);
//                    indata.pImageBuffer = intByReference;
                    continue;
                }
                if (detectionAttempts < 1) {
                    logger.info("ANPR: NO PLATE");
                }
                loopAgain = false;
            }
            mat1.release();
        }
        catch (Exception e) {
            logger.error("Error during ANPR recognition: {}", (Throwable)e);
        }
        return plateNumbers;
    }*/



