package com.si.anpr;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MatrixResult {
    private static final Logger logger = LoggerFactory.getLogger(MatrixResult.class);

    private final String plateNumber;
    private final String country;
    private final Integer x0;
    private final Integer y0;
    private final Integer x1;
    private final Integer y1;
    private final Integer x2;
    private final Integer y2;
    private final Integer x3;
    private final Integer y3;
    private final Integer confidence;
    private final Integer plateWidth;
    private final Integer plateHeight;

    /**
         sprintf (buffer, "%s|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d|%d",
         outdata->CharBuffer,
         outdata->Country,
         outdata->x0,
         outdata->y0,
         outdata->x1,
         outdata->y1,
         outdata->x2,
         outdata->y2,
         outdata->x3,
         outdata->y3,
         outdata->Confidence,
         outdata->PlateWidth,
         outdata->PlateHeight);
     * @param pipedResult
     */
    public MatrixResult(String pipedResult) {
        logger.info(pipedResult);
        String[] parts = pipedResult.split("\\|");
        plateNumber = parts[0];
        country = resolveCountry(parts[1]);
        x0 = Integer.valueOf(parts[2]);
        y0 = Integer.valueOf(parts[3]);
        x1 = Integer.valueOf(parts[4]);
        y1 = Integer.valueOf(parts[5]);
        x2 = Integer.valueOf(parts[6]);
        y2 = Integer.valueOf(parts[7]);
        x3 = Integer.valueOf(parts[8]);
        y3 = Integer.valueOf(parts[9]);
        confidence = Integer.valueOf(parts[10]);
        plateWidth = Integer.valueOf(parts[11]);
        plateHeight = Integer.valueOf(parts[12]);

    }

    private String resolveCountry(String code) {
        return code;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public String getCountry() {
        return country;
    }

    public Integer getX0() {
        return x0;
    }

    public Integer getY0() {
        return y0;
    }

    public Integer getX1() {
        return x1;
    }

    public Integer getY1() {
        return y1;
    }

    public Integer getX2() {
        return x2;
    }

    public Integer getY2() {
        return y2;
    }

    public Integer getX3() {
        return x3;
    }

    public Integer getY3() {
        return y3;
    }

    public Integer getConfidence() {
        return confidence;
    }

    public Integer getPlateWidth() {
        return plateWidth;
    }

    public Integer getPlateHeight() {
        return plateHeight;
    }
}
