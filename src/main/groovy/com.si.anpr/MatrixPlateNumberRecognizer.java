package com.si.anpr;


import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

/**
 * Created by martoniczd on 2016. 12. 28..
 */
public class MatrixPlateNumberRecognizer {

    private static final Logger logger = LoggerFactory.getLogger(MatrixPlateNumberRecognizer.class);

    static {
        try {
            System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            nu.pattern.OpenCV.loadShared();

        } catch (Exception e) {
            logger.error("OpenCV Native load exception ", e);
        }
    }

    private Cardet anpr;

    public MatrixPlateNumberRecognizer() {
        anpr = new Cardet();
        anpr.Init(2);

    }

    public MatrixResult recognize(BufferedImage img) {
        Cardet.ENGINE_INDATA indata = new Cardet.ENGINE_INDATA();
        byte[] data = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
        Mat mat = new org.opencv.core.Mat(img.getHeight(), img.getWidth(), org.opencv.core.CvType.CV_8UC3);
        mat.put(0, 0, data);
        Mat mat1 = new Mat(img.getHeight(), img.getWidth(), CvType.CV_8UC1);
        Imgproc.cvtColor(mat, mat1, Imgproc.COLOR_RGB2GRAY);
        byte[] data1 = new byte[mat1.rows() * mat1.cols() * (int) (mat1.elemSize())];
        mat1.get(0, 0, data1);
        indata.AdvancedOptions = 0;
        indata.AspectRatioX = 1;
        indata.AspectRatioY = 1;
        indata.bDetectSquarePlates = 1;
        indata.bDetectWhiteOnBlack = 1;
        indata.bUseLightCorrection = 1;
        indata.ImageBufferSpan = img.getWidth();
        indata.MultipleRecognLimit = 2;
        indata.ImageHeight = img.getHeight();
        indata.ImageWidth = img.getWidth();
        MatrixResult result = new MatrixResult(this.anpr.ProcessImage(data1, img.getWidth(), img.getHeight()));
        mat.release();
        mat1.release();
        logger.debug("Recognized: " + result.toString());
        return result;
    }

}
