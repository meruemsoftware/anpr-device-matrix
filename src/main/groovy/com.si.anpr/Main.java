package com.si.anpr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String... args) throws IOException {
        MatrixPlateNumberRecognizer recognizer = new MatrixPlateNumberRecognizer();
        MatrixResult matrixResult = recognizer.recognize(ImageIO.read(new File(args[0])));
        logger.info(matrixResult.toString());
    }
}
